<?php
require "cnx.php";
require "begin.html";
?>
<h1> List of the last 25 nobel prizes </h1>
<?php require "last25.php" ?>


<p> Welcome to the website listing the different nobel prizes given until 2010. It references <strong> <?php require 'count.php' ?></strong>
 nobel prizes. You can modify this list by adding new Nobel Prizes, and removing or updating the information contained in this database. 
 You can also search among the nobel prizes on the name, the year and the different categories. </p>


<?php require "end.html"; ?>